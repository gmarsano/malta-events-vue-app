import axios from "axios";

const state = {
  cities: [],
};

const getters = {
  getCities: (state) => state.cities,
};

const actions = {
  async fetchCities({ commit }) {
    const response = await axios.get("/cities");
    commit("setCities", response.data);
  },

  async addCity({ commit }, city) {
    console.log(city.display_name);
    const response = await axios.post("/cities", city);
    commit("newCity", response.data);
  },

  async deleteCity({ commit }, id) {
    await axios.delete(`/cities/${id}`);
    commit("removeCity", id);
  },

  async updateCity({ commit }, updatedCity) {
    const response = await axios.patch(
      `/cities/${updatedCity.id}`,
      updatedCity
    );
    commit("updateCity", response.data);
  },
};

const mutations = {
  setCities: (state, cities) => (state.cities = cities),
  newCity: (state, city) => state.cities.unshift(city),
  removeCity: (state, id) =>
    (state.cities = state.cities.filter((city) => city.id !== id)),
  updateCity: (state, updatedCity) => {
    const index = state.cities.findIndex((city) => city.id === updatedCity.id);
    if (index !== -1) {
      state.cities.splice(index, 1, updatedCity);
    }
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
