import { createStore } from "vuex";
import citiesStore from "./cities";

export default createStore({
  modules: {
    citiesStore,
  },
});
